// SPDX-FileCopyrightText: 2022 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{} // use default options

func main() {
	brokerAddress := func() string {
		if a, ok := os.LookupEnv(`BROKER_ADDRESS`); ok {
			return a
		}

		return `broker:9092`
	}()

	maxMessageSize := func() int {
		a, ok := os.LookupEnv(`MAX_MESSAGE_SIZE`)
		if !ok {
			return 4 * 1024 * 1024 // 4MB
		}

		v, err := strconv.ParseInt(a, 10, 32)
		if err != nil {
			log.Panicf("Could not parse MAX_MESSAGE_SIZE: %v", err)
		}

		return int(v)
	}()

	// TODO: set up supported methods for each endpoint
	router := mux.NewRouter()

	router.Handle("/v1/topics", &listTopicsHandler{brokerAddress: brokerAddress})
	router.Handle("/v1/consume/{topic}", &consumeHandler{brokerAddress: brokerAddress})
	router.Handle("/v1/subscribe/{topic}", &subscribeHandler{brokerAddress: brokerAddress})
	router.Handle("/v1/push/{topic}", &pushHandler{brokerAddress: brokerAddress, maxMessageSize: maxMessageSize})
	router.Handle("/v1/publish/{topic}", &publishHandler{brokerAddress: brokerAddress})

	log.Fatal(http.ListenAndServe(os.Getenv("LISTEN_ADDRESS"), router))
}
