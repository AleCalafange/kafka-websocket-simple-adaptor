// SPDX-FileCopyrightText: 2022 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"encoding/json"
	"log"
	"net/http"
	"sort"

	kafka "github.com/segmentio/kafka-go"
)

type listTopicsHandler handler

func (h *listTopicsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	topics, err := func() ([]string, error) {
		conn, err := kafka.Dial("tcp", h.brokerAddress)
		if err != nil {
			log.Printf(`Error listing topics: %v`, err)
			return nil, err
		}

		defer conn.Close()

		partitions, err := conn.ReadPartitions()
		if err != nil {
			log.Printf(`Error listing topics: %v`, err)
			return nil, err
		}

		m := map[string]bool{}

		for _, p := range partitions {
			m[p.Topic] = true
		}

		topics := make([]string, 0, len(m))

		for t := range m {
			if t != `__consumer_offsets` {
				topics = append(topics, t)
			}
		}

		sort.Strings(topics)

		return topics, nil
	}()

	if err != nil {
		log.Printf(`Error listing topics: %v`, err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	w.Header().Add(`Content-Type`, `application/json`)

	_ = json.NewEncoder(w).Encode(topics)
}
