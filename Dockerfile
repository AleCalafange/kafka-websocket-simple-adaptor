FROM registry.gitlab.com/lightmeter/golang-builder-docker-image:latest AS builder

ADD . /src

WORKDIR /src

RUN go build \
	-o "kafka-ws-adaptor" \
	-ldflags "-linkmode external -extldflags '-static' -s -w" \
	-a -v

FROM scratch

COPY --from=builder /src/kafka-ws-adaptor /kafka-ws-adaptor
COPY --from=builder /usr/share/ca-certificates /usr/share/ca-certificates

ENV SSL_CERT_DIR /usr/share/ca-certificates/mozilla

ENTRYPOINT ["/kafka-ws-adaptor"]
