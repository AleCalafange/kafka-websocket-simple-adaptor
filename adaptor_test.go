package main

import (
	"errors"
	"net/http"
	"testing"

	kafka "github.com/segmentio/kafka-go"
	. "github.com/smartystreets/goconvey/convey"
)

func TestErrorCodes(t *testing.T) {
	Convey("Error Codes", t, func() {
		So(statusCodeFromError(kafka.MessageSizeTooLarge), ShouldEqual, http.StatusRequestEntityTooLarge)
		So(statusCodeFromError(kafka.BrokerNotAvailable), ShouldEqual, http.StatusInternalServerError)
		So(statusCodeFromError(nil), ShouldEqual, http.StatusOK)
		So(statusCodeFromError(errors.New(`Something Else`)), ShouldEqual, http.StatusInternalServerError)
		So(statusCodeFromError(kafka.WriteErrors{kafka.MessageSizeTooLarge}), ShouldEqual, http.StatusRequestEntityTooLarge)
		So(statusCodeFromError(kafka.WriteErrors{}), ShouldEqual, http.StatusInternalServerError)
		So(statusCodeFromError(kafka.WriteErrors{kafka.BrokerNotAvailable}), ShouldEqual, http.StatusInternalServerError)
		So(statusCodeFromError(kafka.WriteErrors{kafka.BrokerNotAvailable, kafka.MessageSizeTooLarge}), ShouldEqual, http.StatusRequestEntityTooLarge)
	})
}
