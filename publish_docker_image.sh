#!/bin/sh

set -o pipefail
set -e
set -vx

cat > /kaniko/.docker/config.json << EOF
{
  "auths": {
    "$CI_REGISTRY":{
      "username":"$CI_REGISTRY_USER",
      "password":"$CI_REGISTRY_PASSWORD"
    }
  }
}"
EOF

mkdir -p .docker-cache

function build_tag() {
  # allows us to publish from mailserver from any branch!
  if [ "$CI_COMMIT_REF_NAME" == "master" ]; then
    echo "latest"
    return
  fi

  echo "$CI_COMMIT_REF_NAME" | tr -d '/'
}

/kaniko/executor \
  --context $CI_PROJECT_DIR \
  --dockerfile $CI_PROJECT_DIR/Dockerfile \
  --destination "$CI_REGISTRY_IMAGE:$(date '+%Y%m%d-%H%M%S')" \
  --destination "$CI_REGISTRY_IMAGE:$(build_tag)" \
  --build-arg "GIT_TAG=$CI_COMMIT_TAG" \
  --cache-dir .docker-cache
