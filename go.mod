module kafka-ws-adaptor

go 1.19

require (
	github.com/buger/jsonparser v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.5.0
	github.com/jstemmer/go-junit-report v1.0.0
	github.com/segmentio/kafka-go v0.4.36
	github.com/smartystreets/goconvey v1.7.2
	github.com/xhit/go-str2duration/v2 v2.0.0
)

require (
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
	github.com/smartystreets/assertions v1.2.0 // indirect
)
