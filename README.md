# Experimental Kafka-Websocket adaptor

This is a small test server that exposed Kafka publish/subscribe features in a very stripped down
way. Only publishing and subscribing to a single topic is possible.

This server exposes two websocket endpoints:
- /v1/subscribe/my-topic
- /v1/publish/my-topic

And the HTTP endpoints:
- POST /v1/push/my-topic
- GET /v1/consume/my-topic
- GET /v1/topics

To test it, you can use the provided docker-compose-test.yml file:

```
$ docker compose -f docker-compose-test.yml build
$ docker compose -f docker-compose-test.yml up -d
```

Now you can use `curl` and `wscat` for instance, to access the endpoints in the respective protocols:

```
$ wscat --connect 'ws://127.0.0.1:9999/v1/subscribe/test'
```

You can test the producer with:

```
$ wscat --connect 'ws://127.0.0.1:9999/v1/publish/test'
```

You can also publish simple messages via HTTP without websockets, one message per line in the posted data.

```
$ curl -X POST --data-binary @- 'http://127.0.0.1:9999/v1/push/test' <<< "My Message here"
```

Anything you type in the producer/publishers will go through Kafka and echoed in the subscribers, in real time.

## Arguments

For /push and /publish, it's possible to override the message timestamp by passing an extra GET argument `message-time`,
such as `?message-time=1667667891`. The RFC3339 syntax for the value is also possible.

In case the message payload is a valid JSON encoded message, it's possible to extract the timestamp from such payload.
You can specify the "path" for the element in the JSON message by passing the `message-time-json-path` GET argument,
such as on `?message-time-json-path?=metadata.ts`, in case your messages look like:

```
{ "metadata": {"ts": 1667671163 }, "data": "Hello" }
```

Please note that setting a timestamp time older than the most recent timestamp in the topic will mess up with
any consumers or subscribers that try to offset by time, as they will be out of order.

Use it with caution.

For /consume and /subscribe, the following GET parameters are valid:

Those are GET parameters supported to specify the interval of values to read,
in order of priority:

- from: Unix timestamp, or RFC33339 (with or without nanoseconds) string, or "clock" time,
  such as 2:45PM or 11:34:56, interpreted in the server local time. It also accepts the syntax compatible
  with Go time.Duration. For instance, using 2h3m means "two hours ago, 3 minutes ago".
  It allows specify when to start reading.

- to: same syntax as `from`. It allows specifying an upper time limit on
  reading messages. For the /subscribe endpoint, when set in the future,
  it defines when the subscription should stop.

- tail:
    an integer specifing how many of the most recent messages should be
    read. For instance, tail=10 means that the subscriber or consumer
    will start from the 10th most recent message.
    It can also be set to a string `first` in case you want to read from the oldest
    message in the topic. Or `latest`, meaning start reading after the most recent message.
    This argument tries to emulate the semantics of the unix `tail` program.
    Defaults to `latest`.
    Counterintuitively, tail=0 has the same effect as tail=first.

# TODO

- Handle errors properly to the client

- /v1/delete_topic endpoint

- Support head=10 argument on consume, meaning read only the first 10 messages from the chosen topic.

- OpenAPI/Swagger style documentation

- Automated tests

- Support for multiple brokers

- Support consumer group and partitions

- Support subscribing to multiple topics

- Support for more complex and common Kafka use cases, and not be restricted to pubsub

- Support for binary encoded messages (right now there's only support for UTF-8 encoded messages)

- Maybe implement authentication?

- Tune performance, which for now is expected to be bad
