// SPDX-FileCopyrightText: 2022 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	kafka "github.com/segmentio/kafka-go"
)

type pushHandler handler

func (h *pushHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	topic := mux.Vars(r)[`topic`]

	getMessageTimestamp, err := buildMessageTimestampStrategy(r.URL.Query())
	if err != nil {
		log.Printf(`Error pushing: %v`, err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	kafkaWriter := createKafkaWriter(h.brokerAddress, topic, h.maxMessageSize)

	defer kafkaWriter.Close()

	defer r.Body.Close()

	scanner := bufio.NewScanner(r.Body)

	scanBuffer := make([]byte, 0, h.maxMessageSize)
	scanner.Buffer(scanBuffer, h.maxMessageSize-2)

	for scanner.Scan() {
		value := scanner.Bytes()

		ts, err := getMessageTimestamp(value)
		if err != nil {
			if err != nil {
				log.Printf(`Error pushing: %v`, err)
				w.WriteHeader(http.StatusBadRequest)

				return
			}
		}

		if err := kafkaWriter.WriteMessages(r.Context(), kafka.Message{
			Value: value,
			Time:  ts,
		}); err != nil {
			log.Printf(`Error pushing message with length %d: %v`, len(value), err)

			w.WriteHeader(statusCodeFromError(err))

			_, _ = fmt.Fprintf(w, `Error pushing message with length %d: %v`, len(value), err)

			return
		}
	}

	if err := scanner.Err(); err != nil {
		log.Println("error writing kafka message:", err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}
}
