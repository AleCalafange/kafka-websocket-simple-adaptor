// SPDX-FileCopyrightText: 2022 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/buger/jsonparser"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	kafka "github.com/segmentio/kafka-go"
	str2duration "github.com/xhit/go-str2duration/v2"
)

func max(a, b int64) int64 {
	if a > b {
		return a
	}

	return b
}

func tryToSetKafkaReaderOffset(ctx context.Context, brokerAddr, topic string, reader *kafka.Reader, maxNumberOfMessagesToFetch int64, timeOffset time.Time) (int64, error) {
	conn, err := kafka.DefaultDialer.DialLeader(ctx, "tcp", brokerAddr, topic, 0)
	if err != nil {
		log.Printf(`Error setting offset: %v`, err)
		return 0, err
	}

	defer conn.Close()

	firstOffset, lastOffset, err := conn.ReadOffsets()
	if err != nil {
		log.Printf(`Error setting offset: %v`, err)
		return 0, err
	}

	switch {
	case !timeOffset.IsZero():
		if err := reader.SetOffsetAt(ctx, timeOffset); err != nil {
			log.Printf(`Error setting offset: %v`, err)
			return 0, err
		}
	case maxNumberOfMessagesToFetch != 0:
		absoluteOffset := func() int64 {
			if maxNumberOfMessagesToFetch == 0 {
				return firstOffset
			}

			if maxNumberOfMessagesToFetch == kafka.LastOffset {
				return lastOffset
			}

			o := lastOffset - maxNumberOfMessagesToFetch

			return max(o, firstOffset)
		}()

		if err := reader.SetOffset(absoluteOffset); err != nil {
			log.Printf(`Error setting offset: %v`, err)
			return 0, err
		}
	}

	return lastOffset, nil
}

func tryToParseKitchenTime(s string) (time.Time, error) {
	if t, err := time.Parse(time.Kitchen, s); err == nil {
		return t, nil
	}

	if t, err := time.Parse("15:04:05", s); err == nil {
		return t, nil
	}

	return time.Time{}, nil
}

func tryToParseTime(s string) (time.Time, error) {
	// RFC3339 with nanoseconds, the way Go encodes time.Time to JSON.
	// This option is evaluated first as it's the most common case for us
	// on reading time from JSON encoded payloads by the Go encoding/json library
	if t, err := time.Parse(time.RFC3339Nano, s); err == nil {
		return t, nil
	}

	// no time at all
	if len(s) == 0 {
		return time.Time{}, nil
	}

	// Time can be a unix timestamp
	if ts, err := strconv.ParseInt(s, 10, 64); err == nil {
		return time.Unix(ts, 0), nil
	}

	// As duration, backward in time, from now, meaning `2h` == 2 hours ago
	if d, err := str2duration.ParseDuration(s); err == nil {
		return time.Now().Add(-d), nil
	}

	// RFC3339 formated time
	if t, err := time.Parse(time.RFC3339, s); err == nil {
		return t, nil
	}

	// Finally, a simplified, human readable, such as 3:40PM or 15:40:43, for today, in local time
	if kitchenTime, err := tryToParseKitchenTime(s); err == nil {
		now := time.Now().Local()
		midnight := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local)
		kitchenTimeDuration := time.Duration(60*60*kitchenTime.Hour()+60*kitchenTime.Minute()+kitchenTime.Second()) * time.Second

		return midnight.Add(kitchenTimeDuration).Local(), nil
	}

	// The time cannot be parsed
	return time.Time{}, fmt.Errorf(`Invalid time: "%s"`, s)
}

func maybeContextUntilTime(ctx context.Context, timeOffsetUntil time.Time) (context.Context, context.CancelFunc) {
	// No deadline
	if timeOffsetUntil.IsZero() {
		return ctx, func() {}
	}

	return context.WithDeadline(ctx, timeOffsetUntil)
}

func getReaderArgs(r *http.Request) (string, int64, time.Time, time.Time, error) {
	topic := mux.Vars(r)[`topic`]
	args := r.URL.Query()

	timeOffsetSince, err := tryToParseTime(args.Get(`from`))
	if err != nil {
		log.Printf(`Error parsing args: %v`, err)
		return "", 0, time.Time{}, time.Time{}, err
	}

	timeOffsetUntil, err := tryToParseTime(args.Get(`to`))
	if err != nil {
		log.Printf(`Error parsing args: %v`, err)
		return "", 0, time.Time{}, time.Time{}, err
	}

	msgOffset, err := parseOffset(args.Get(`tail`))
	if err != nil {
		log.Printf(`Error parsing args: %v`, err)
		return "", 0, time.Time{}, time.Time{}, err
	}

	if !timeOffsetUntil.IsZero() && timeOffsetUntil.Before(timeOffsetSince) {
		log.Printf(`Error parsing args: %v`, err)
		return "", 0, time.Time{}, time.Time{}, fmt.Errorf("Invalid time interval: %w", err)
	}

	return topic, msgOffset, timeOffsetSince, timeOffsetUntil, nil
}

func parseOffset(s string) (int64, error) {
	switch s {
	case "first":
		return 0, nil
	case "":
		fallthrough
	case "latest":
		return kafka.LastOffset, nil
	default:
		offset, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			log.Printf(`Error parsing args: %v`, err)
			return 0, err
		}

		return offset, nil
	}
}

func createKafkaWriter(brokerAddress, topic string, maxSize int) *kafka.Writer {
	kafkaWriter := kafka.NewWriter(kafka.WriterConfig{
		Brokers:    []string{brokerAddress},
		Balancer:   &kafka.RoundRobin{},
		BatchSize:  1,
		BatchBytes: maxSize,
		Topic:      topic,
	})

	// this will cause the reader to fail writing the first message,
	// we the partition is still being created
	// so the caller should try again.
	kafkaWriter.AllowAutoTopicCreation = true

	return kafkaWriter
}

func buildMessageTimestampStrategy(args url.Values) (func([]byte) (time.Time, error), error) {
	// Allow extracting the message timestamp from the payload, in case it's a valid json
	// Pass the field path, compatible with this library:
	// https://github.com/buger/jsonparser
	// where the path is a series of components separated by dot `.`
	// For instance, if the payload is this:
	/// { "metadata": {"ts": 1667671163 }, "data": "Hello" }
	// You can pass "metadata.ts" as path
	if rawPath := args.Get(`message-time-json-path`); len(rawPath) > 0 {
		path := strings.Split(rawPath, `.`)

		return func(value []byte) (time.Time, error) {
			v, _, _, err := jsonparser.Get(value, path...)
			if err != nil {
				log.Printf(`Error getting timestamp: %v`, err)
				return time.Time{}, err
			}

			return tryToParseTime(string(v))
		}, nil
	}

	// Use a constant value, passed as argument, for the timestamp.
	if ts := args.Get(`message-time`); len(ts) > 0 {
		customMessageTime, err := tryToParseTime(ts)
		if err != nil {
			log.Printf(`Error getting timestamp: %v`, err)
			return nil, err
		}

		return func([]byte) (time.Time, error) {
			return customMessageTime, nil
		}, nil
	}

	// Otherwise don't set the timestamp at all
	return func([]byte) (time.Time, error) {
		return time.Time{}, nil
	}, nil
}

type handler struct {
	brokerAddress  string
	maxMessageSize int
}

var (
	pingMsg *websocket.PreparedMessage
)

func init() {
	var err error

	pingMsg, err = websocket.NewPreparedMessage(websocket.PingMessage, []byte("Ping to keep the connection alive!"))

	if err != nil {
		panic(err)
	}
}

func statusCodeFromError(err error) int {
	if err == nil {
		return http.StatusOK
	}

	if errors.Is(err, kafka.MessageSizeTooLarge) {
		return http.StatusRequestEntityTooLarge
	}

	var writeErrors kafka.WriteErrors

	if !errors.As(err, &writeErrors) {
		return http.StatusInternalServerError
	}

	for _, writeErr := range writeErrors {
		if errors.Is(writeErr, kafka.MessageSizeTooLarge) {
			return http.StatusRequestEntityTooLarge
		}
	}

	return http.StatusInternalServerError
}
