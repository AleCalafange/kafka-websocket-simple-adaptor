// SPDX-FileCopyrightText: 2022 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"errors"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	kafka "github.com/segmentio/kafka-go"
)

type subscribeHandler handler

func (h *subscribeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	topic, offset, timeOffsetSince, timeOffsetUntil, err := getReaderArgs(r)
	if err != nil {
		log.Printf(`Error subscribing: %v`, err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	kafkaReader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:     []string{h.brokerAddress},
		Topic:       topic,
		StartOffset: kafka.LastOffset,
	})

	defer kafkaReader.Close()

	if _, err := tryToSetKafkaReaderOffset(r.Context(), h.brokerAddress, topic, kafkaReader, offset, timeOffsetSince); err != nil {
		log.Printf(`Error subscribing: %v`, err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	wsClient, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf(`Error subscribing: %v`, err)
		return
	}

	defer wsClient.Close()

	for {
		err := func() error {
			// try to send a ping msg every 10 seconds to keep the connection alive
			pingTimeoutContext, cancelCtx := context.WithTimeout(r.Context(), time.Second*10)
			defer cancelCtx()

			m, err := func() (kafka.Message, error) {
				ctx, cancelCtx := maybeContextUntilTime(pingTimeoutContext, timeOffsetUntil)
				defer cancelCtx()

				return kafkaReader.ReadMessage(ctx)
			}()

			if err := pingTimeoutContext.Err(); err != nil && errors.Is(err, context.DeadlineExceeded) {
				if err := wsClient.WritePreparedMessage(pingMsg); err != nil {
					return err
				}

				return nil
			}

			if err != nil {
				return err
			}

			if err := wsClient.WriteMessage(websocket.TextMessage, m.Value); err != nil {
				return err
			}

			return nil
		}()

		if err != nil {
			log.Printf(`Error subscribing: %v`, err)
			return
		}
	}
}
