// SPDX-FileCopyrightText: 2022 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"bufio"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	kafka "github.com/segmentio/kafka-go"
)

type publishHandler handler

func (h *publishHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	topic := mux.Vars(r)[`topic`]

	getMessageTimestamp, err := buildMessageTimestampStrategy(r.URL.Query())
	if err != nil {
		log.Printf(`Error publishing: %v`, err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	kafkaWriter := createKafkaWriter(h.brokerAddress, topic, h.maxMessageSize)

	defer kafkaWriter.Close()

	wsClient, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf(`Error publishing: %v`, err)
		return
	}

	defer wsClient.Close()

	const bufferSize = 4 * 1024 * 1024 // 4MB max message size!
	scanBuffer := make([]byte, 0, bufferSize)

	for {
		_, reader, err := wsClient.NextReader()
		if err != nil {
			log.Printf(`Error publishing: %v`, err)
			return
		}

		scanner := bufio.NewScanner(reader)
		scanner.Buffer(scanBuffer, bufferSize-2)

		for scanner.Scan() {
			ts, err := getMessageTimestamp(scanner.Bytes())
			if err != nil {
				if err != nil {
					log.Printf(`Error publishing: %v`, err)
					w.WriteHeader(http.StatusBadRequest)

					return
				}
			}

			if err := kafkaWriter.WriteMessages(r.Context(), kafka.Message{
				Value: scanner.Bytes(),
				Time:  ts,
			}); err != nil {
				log.Printf(`Error publishing: %v`, err)
				w.WriteHeader(statusCodeFromError(err))

				return
			}
		}

		if err := scanner.Err(); err != nil {
			log.Printf(`Error publishing: %v`, err)
			w.WriteHeader(http.StatusBadRequest)

			return
		}
	}
}
