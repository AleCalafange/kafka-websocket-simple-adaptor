// SPDX-FileCopyrightText: 2022 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"bufio"
	"log"
	"net/http"
	"time"

	kafka "github.com/segmentio/kafka-go"
)

type consumeHandler handler

func (h *consumeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	topic, maxNumberOfMessagesToFetch, timeOffsetSince, timeOffsetUntil, err := getReaderArgs(r)
	if err != nil {
		log.Printf(`Error consuming: %v`, err)
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	kafkaReader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:     []string{h.brokerAddress},
		Topic:       topic,
		StartOffset: kafka.LastOffset,
		MaxWait:     time.Millisecond * 100,
	})

	defer kafkaReader.Close()

	lastOffset, err := tryToSetKafkaReaderOffset(r.Context(), h.brokerAddress, topic, kafkaReader, maxNumberOfMessagesToFetch, timeOffsetSince)
	if err != nil {
		log.Printf(`Error consuming: %v`, err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	w.Header().Add(`Content-Type`, `application/x-ndjson`)

	// w.Write() is expensive, so bufferizing should improve
	// the output performance considerably
	bufferedOutput := bufio.NewWriterSize(w, 4096)

	defer bufferedOutput.Flush()

	for {
		currentOffset := kafkaReader.Offset()

		if currentOffset >= lastOffset || currentOffset == kafka.LastOffset {
			return
		}

		m, err := kafkaReader.ReadMessage(r.Context())
		if err != nil {
			log.Printf(`Error consuming: %v`, err)
			return
		}

		if !timeOffsetUntil.IsZero() && m.Time.After(timeOffsetUntil) {
			return
		}

		if _, err := bufferedOutput.Write(m.Value); err != nil {
			log.Printf(`Error consuming: %v`, err)
			return
		}

		if _, err := bufferedOutput.Write([]byte("\n")); err != nil {
			log.Printf(`Error consuming: %v`, err)
			return
		}
	}
}
